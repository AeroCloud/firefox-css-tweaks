# Mozilla CSS Tweaks

To install : 
- Click on download (button on the left of "clone") on the right below the project's title, then click zip
- Open the zip with 7zip or Winrar *(you should use 7zip BTW it's basically the same but free, so without the annoying pop-up)*
- Back on Firefox type "about:config" in the address bar, then search for "toolkit.legacyUserProfileCustomizations.stylesheets" and make sure it is set to true. If it's not, double click on it to toggle
- Now type "about:support" in the address. In the table, search for the line named "Profile folder" and then click on the button "open folder" on said line
- In the folder opened in the previous step, create a folder named "chrome"
- move all the files from the repo *save for the README.md* into said folder
- You're done